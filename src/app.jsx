/* eslint-env browser */

import React, { Component } from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";

import Main from "./routes/main/index.jsx";
import players from "@services/players/reducer";
import { watchSagas } from "@services";

const sagaMiddleware = createSagaMiddleware();
let enhancer = applyMiddleware(sagaMiddleware);

if (process.env.NODE_ENV === "development") {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  enhancer = composeEnhancers(enhancer);
}

const reducer = combineReducers({
  players
});

const store = createStore(reducer, enhancer);

sagaMiddleware.run(watchSagas);

class App extends Component<Object, Object> {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
