// @flow

export type StateType = {
  stats: Array<any>
};

export type PropsType = {
  players: Array<any>,
  mystats: {
    GP: 0,
    MIN: 0,
    PTS: 0,
    REB: 0,
    AST: 0,
    STL: 0
  },

  getPlayers(): Promise<any>,
  getMyStats(): Promise<any>
};
