// @flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import List from "react-virtualized/dist/commonjs/List";
import AutoSizer from "react-virtualized/dist/commonjs/AutoSizer";
import { Jumbotron, Container, Row, Col } from "reactstrap";

import { getPlayers, getMyStats } from "@services/players/actions";
import type { PropsType, StateType } from "./models";

import "react-virtualized/styles.css";
import "bootstrap/dist/css/bootstrap.min.css";

class Main extends Component<PropsType, StateType> {
  constructor(props: any) {
    super(props);
    this.state = {
      stats: []
    };
  }

  componentDidMount() {
    const { getPlayers, getMyStats } = this.props;

    getPlayers();
    getMyStats();
  }

  static getDerivedStateFromProps(props: any) {
    let arr = props.players;
    if (props.mystats.PTS) {
      arr.push(props.mystats);
    }

    arr.sort((a, b) => {
      if (a.PTS < b.PTS) {
        return 1;
      } else {
        return -1;
      }
    });

    return { stats: arr };
  }

  render() {
    const { stats } = this.state;
    const { mystats } = this.props;

    return (
      <section id="main">
        <Container>
          <Jumbotron>
            <AutoSizer disableHeight>
              {({ width }) => (
                <Fragment>
                  <Row style={{ width: width - 16, backgroundColor: "#999" }}>
                    <Col xs="1">RANK</Col>
                    <Col xs="5">PLAYER</Col>
                    <Col xs="1">GP</Col>
                    <Col xs="1">PTS</Col>
                    <Col xs="1">AST</Col>
                    <Col xs="1">STL</Col>
                  </Row>
                  <br />
                  <List ref="List" height={300} width={width} rowCount={stats.length} rowHeight={50} rowRenderer={data => this._rowRenderer(data)} />

                  <Row style={{ width: width - 16, backgroundColor: "#fff" }}>
                    <Col xs="1">{this._find(stats, mystats.PLAYER) + 1}</Col>
                    <Col xs="5">{mystats.PLAYER}</Col>
                    <Col xs="1">{mystats.GP}</Col>
                    <Col xs="1">{mystats.PTS}</Col>
                    <Col xs="1">{mystats.AST}</Col>
                    <Col xs="1">{mystats.STL}</Col>
                  </Row>
                </Fragment>
              )}
            </AutoSizer>
          </Jumbotron>
        </Container>
      </section>
    );
  }

  _rowRenderer(data: Object) {
    const { stats } = this.state;
    const { mystats } = this.props;

    return (
      <Row style={data.style} key={data.key}>
        <Col xs="1">{data.index + 1}</Col>
        <Col xs="5" style={mystats.PLAYER == stats[data.index].PLAYER ? { color: "#f00" } : null}>
          {stats[data.index].PLAYER}
        </Col>
        <Col xs="1">{stats[data.index].GP}</Col>
        <Col xs="1">{stats[data.index].PTS}</Col>
        <Col xs="1">{stats[data.index].AST}</Col>
        <Col xs="1">{stats[data.index].STL}</Col>
      </Row>
    );
  }

  _find(array: Array<any>, value: string) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].PLAYER === value) return i;
    }
    return -1;
  }
}

export default connect(
  state => ({
    players: state.players.players,
    mystats: state.players.mystats
  }),
  {
    getPlayers: getPlayers,
    getMyStats: getMyStats
  }
)(Main);
