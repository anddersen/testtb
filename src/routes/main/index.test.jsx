// @flow

import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Main from "./index.jsx";

Enzyme.configure({ adapter: new Adapter() });

describe("Main", () => {
  describe("component", () => {
    let shallowed;

    beforeEach(() => {
      shallowed = shallow(<PrivateRoute.WrappedComponent component={Main} />);
    });
    it("render", () => {
      expect(shallowed).toBeDefined();
    });
  });
});
