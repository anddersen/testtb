import { GET_MYSTATS, GET_PLAYERS } from "./constants";

export function getPlayers() {
  return {
    type: GET_PLAYERS
  };
}

export function getMyStats() {
  return {
    type: GET_MYSTATS
  };
}
