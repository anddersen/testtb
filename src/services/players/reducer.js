// @flow
import { GET_PLAYERS, GET_MYSTATS } from "./constants";

type State = {
  players: Array<any>,
  mystats: {
    GP: Number,
    MIN: String,
    PTS: Number,
    REB: Number,
    AST: Number,
    STL: Number
  }
};
const initialState: State = {
  players: [],
  mystats: {
    PLAYER: "no user",
    GP: 0,
    PTS: 0,
    REB: 0,
    AST: 0,
    STL: 0
  }
};

export default function players(
  state: State = initialState,
  action: {
    type: string,
    payload: State
  }
): State {
  switch (action.type) {
    case `${GET_PLAYERS}_SUCCESS`: {
      return {
        ...state,
        players: action.payload.players
      };
    }

    case `${GET_MYSTATS}_SUCCESS`: {
      return {
        ...state,
        mystats: action.payload.mystats
      };
    }

    default:
      return state;
  }
}
