// @flow

import { getPlayers, getMyStats } from "./actions";
import { GET_PLAYERS, GET_MYSTATS } from "./constants";

describe("actions", () => {
  it("jest for getPlayers; get stats of players", () => {
    expect(getPlayers()).toEqual({
      type: GET_PLAYERS
    });
  });
  it("jest for getMyStats; get stats of user", () => {
    expect(getMyStats()).toEqual({
      type: GET_MYSTATS
    });
  });
});
