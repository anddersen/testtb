// @flow

import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import type { Effect } from "redux-saga";

import { GET_PLAYERS, GET_MYSTATS } from "./constants";
import { PLAYESR } from "./mock";

export function* getPlayersSaga(): Generator<*, *, *> {
  const url = "http://localhost:3000/players";

  const mock = new MockAdapter(axios);

  const result = PLAYESR.resultSet.rowSet.map(e => {
    return {
      PLAYER: e[2],
      GP: e[4],
      PTS: e[e.length - 2],
      AST: e[e.length - 7],
      STL: e[e.length - 6]
    };
  });

  mock.onGet(url).reply(200, {
    players: result
  });

  try {
    const response = yield call(axios.get, url);
    const payload = response.data;

    yield put({ type: `${GET_PLAYERS}_SUCCESS`, payload });
  } catch (error) {
    yield put({ type: `${GET_PLAYERS}_FAILURE` });
  }
}

export function* getMyStatsSaga(): Generator<*, *, *> {
  const url = "http://localhost:3000/mystats";

  const mock = new MockAdapter(axios);
  mock.onGet(url).reply(200, {
    mystats: {
      PLAYER: "I am USER",
      GP: 3,
      PTS: 17,
      REB: 9,
      AST: 4,
      STL: 7
    }
  });

  try {
    const response = yield call(axios.get, url);
    const payload = response.data;

    yield put({ type: `${GET_MYSTATS}_SUCCESS`, payload });
  } catch (error) {
    yield put({ type: `${GET_MYSTATS}_FAILURE` });
  }
}

export const playersSaga: Array<Effect> = [takeLatest(GET_PLAYERS, getPlayersSaga), takeLatest(GET_MYSTATS, getMyStatsSaga)];
