// @flow
import { all } from "redux-saga/effects";

import { playersSaga } from "./players/saga";

export function* watchSagas(): Generator<*, *, *> {
  yield all([].concat(playersSaga));
}
