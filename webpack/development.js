const webpack = require("webpack");
const path = require("path");
const merge = require("webpack-merge");

const common = require("./common");

module.exports = merge(common, {
  mode: "development",
  devServer: {
    contentBase: path.resolve(__dirname, "../src"),
    hot: true,
    historyApiFallback: true,
    port: 3000,
    open: true
  },
  devtool: "source-map",
  plugins: [new webpack.HotModuleReplacementPlugin(), new webpack.NamedModulesPlugin()]
});
